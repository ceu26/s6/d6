function addToCart(itemName, itemPrice, itemStocks){
    var name = document.getElementById(itemName).innerHTML;
    var price = document.getElementById(itemPrice).innerHTML;
    price = price.replace("P ", "") 
    var stocks = document.getElementById(itemStocks).innerHTML;
    stocks = stocks.replace("Stocks: ", "");

    var totalPrice = price*1

    console.log("item name ="+name)
    console.log("item price ="+price)
    console.log("item stocks ="+stocks)
    
    var dataArr = [];
    dataArr.push({
        
            "product_name": name,
            "qty": 1,
            "price": price,
            "total_price": totalPrice
        
    })

    var payload = {
        data: dataArr
    }
    var url = `http://localhost:4000/api/insert?table=cart_tbl`
    var content = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    }
    api_client(url, content, (response)=>{
        console.log("add to cart response = "+JSON.stringify(response))
        if (response.successful == true){
            alert("added to cart successfully")
        }
        else{
            alert("Unable to add to cart")
        }
    })

}

function getCartItems(product_name, qty, price, total_price){

    var pname = document.getElementById(product_name).innerHTML;
    var pprice = document.getElementById(price).innerHTML;
    var pqty = document.getElementById(qty).innerHTML;
    var totalPrice = document.getElementById(total_price).innerHTML;
}
